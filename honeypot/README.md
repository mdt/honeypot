Honey Pot
==

Dies ist ein Wlan-Honigtopf, also etwas, das verlockend aussieht und klebrig
ist. Geräte, die in dies Wlan einbuchen, sollen den Eindruck erhalten, Zugriff
auf's Internet zu erhalten aber dabei umfassend beobachtet
werden. Und natürlich so lange zu bleiben wie möglich.

Ziel des Projektes ist die Möglichkeit festzustellen, ob Geräte unerwünschte
Netzwerkaktivitäten tätigen. Zum Beispiel solche, die durch Stalkerware oder
Trojaner unternommen werden oder einfach solche, die Geräte durch ihre
Grundinstallation betreiben.

Das Script muss mit super-user Rechten laufen, da es viele Dinge tut, die
besondere Rechte verlangen. Traust du dem Script nicht, lass es auf einem
Rechner laufen, dessen Installation du hinterher löschen kannst und der keinen
Internetzugriff hat.

Schritt 1a
--

Vorraussetzung ist ein WLAN-Access-Point, der lediglich als Switch
agiert, indem alle Interfaces gebridged sind. Ich nutze OpenWRT, eine
Beschreibung für die Konfiguration ist hier:

[Wireless Access Point / Dumb Access Point / Dumb AP](https://openwrt.org/docs/guide-user/network/wifi/dumbap)

An diesen AP verbindet man einen Rechner per Ethernet und kann nun schauen,
was die Geräte so treiben:

```
tcpdump -i eth0 -vvv
```

Schritt 1b
--

Wenn kein WLAN-Router zur Verfügung steht, kann alternativ der Rechner selbst
als Accesspoint fungieren. Hierzu gibt es das Script `bin/hostapd.sh`, der den
Accesspoint einrichtet. Zugang zum Internet kann dann über eine andere
Netzwerkschnittstelle wie zum Beispiel Ethernet erfolgen.

Schritt 2
--

Als erstes melden sich die Geräte mit einer DHCP Anfrage. Diese muss mit einer
validen IP, einem Standard-Gateway und einem Namens-Server beantwortet werden.

Ich habe keinen für mich brauchbaren und funktionierenden DHCP Server gefunden,
darum habe ich (unter Verwendung von Teilen aus anderen Projekten) einen
eigenen erstellt.

In guter Python-Manier beginnt er zu arbeiten auf `serve_forever()` und ist in
drei Schichten aufgeteilt:

- Senden & Empfangen der UDP Pakete
- Decoden & Encoden der Pakete
- Korrektes Beantworten der Fragen der Geräte

Es beinhaltet nicht die Schicht für die Vergabe der IPs, die im Hauptmodul
kontrolliert wird.

`lib/dhcp.py`

Schritt 3
--

Als nächstes versuchen die Geräte, Namen aufzulösen. Ein DNS Server leistet
dies. Ein einigermassen brauchbares Modul existierte und findet Verwendung. Auch
dieses startet mit `serve_forever()`.

Es beinhaltet nicht die Zuordnung der IPs, die wiederum im Hauptmodul
stattfindet.

Manche Geräte nutzen tcp domain-s (port 853). Dies ist (noch) nicht
implementiert.

`lib/dns.py`

Schritt 4
--

Viele Geräte erfragen dann die Uhrzeit, also sollte NTP gesprochen werden.
Wiederum startet der Dienst mit Aufruf von `serve_forever()`.

Es beinhaltet nicht die Rückgabe der Zeit, diese kommt wiederum aus dem
Hauptmodul.

`lib/ntp.py`

Schritt 5
--

Als erstes Prüfen die Geräte ob das Netz wirklich Zugriff auf das Internet
erlaubt. Dazu erfragen Android-Telefone [generate
204](http://connectivitycheck.gstatic.com/generate_204) oder [generate
204](http://google.com/generate_204) (oder andere ähnliche URLs) und
erwarten einen HTTP-Statuscode von 204. Erst wenn dieser Test erfolgreich war,
nehmen die Telefone an, eine Internet-Verbindung zu haben.

Die Welt spricht HTTP und oft **HTTPS**. Für HTTPS muss die Gegenseite ein
passendes Zertifikat vorweisen, was wir nicht können, wenn wir nicht die
entsprechende Gegenseite sind. Für TLS beinhaltet das HTTPS-Module darum die
Möglichkeit, Zertifikate 'on the fly' zu generieren, die passend sind. Damit
diese akzeptiert werden, muss auf dem Gerät das Root-Zertifikat
`pemdb/open.net-ca-cert.cer` (das beim ersten Start erzeugt wird) installiert
werden.

Das Modul `http_` verwendet den `CertStore` des `mitmproxy.certs`, "mitmproxy"
muss also installiert sein.

`lib/http_.py`

Nächste Schritte
--

Dieses Projekt ist bei weitem nicht fertig, das spannende kommt ja erst.

Da die Geräte so die Zertifikate nicht akzeptieren sollten die 204-Anfragen
ausgeleitet werden, um sie valide zu beantworten. Hierfür kann ein
transparenter SOCKS Proxy implementiert werden. Diese Anfragen werden nicht nur
vom System selbst sondern auch zB vom Firefox gestellt.

Alternativ könnte SSL gebrochen werden, indem auf dem Gerät ein Root-Zertifikat
installiert wird, sodass vom Honeypot ausgestellte Zertifikate akzeptiert
werden. Allerdings sind Apps bekannt, die eigene Zertifikate verwenden und so
nicht mehr funktionieren würden.

DNS over HTTP(S) ist ein weiteres Problem. SNI bietet eine gute Möglichkeit
bereits zu erkennen, was das Gerät vorhat, sodass diese Anfragen blockiert
werden können.

IPv6 ist nicht beachtet.

GUI, die Benutzern zeigt was es tut, a la "Handy gefunden, Anfang der Analyse",
kontaktierte IPs mit farblicher Bewertung etc.

Erkennt, wenn IPs kontaktiert werden, die nicht per DNS über den Honeypot
aufgelöst wurden.

Checkt den Traffic vom Handy gegen die bekannten Spyware Server zB aus
Blacklisten wie
[ioc](https://raw.githubusercontent.com/AssoEchap/stalkerware-indicators/master/ioc.yaml)

Datenschutzkonform, liest also nur Header, nicht Request bodies mit
persönlichen Daten. Speichert keine Personen Bezogenen Daten

Möglichkeit die Spywareliste zu Aktualiesieren, am besten per GUI/Knofdruck
oder beim Startup.

Crasht nicht (zB auf Grund von Traffic, wenn "normale", datenhungige Apps wie
Ticktock geöffnet wird).

Zeigt deutlich, wenn ein Server auf der Blacklist kontaktiert wurde oder
Spyware-Aktivitäten vermutet wurden.

Erkennt, wenn Traffic verdoppelt ist, also ob zB whattsoup Nachrichten an zwei
verschiedene Server gehen

Zeigt an, welche erkannte Spyware-Server es nicht als Adressat gefunden hat.

Projekt sollte auf verfügbarer Hardware laufen (zB Laptop, Desktop, Embedded
etc., Handies oder Tablets scheiden aus, da sie nicht über 2
Netzwerkschnittstellen verfügen).

Installationsanleitung die [jede|techafine] Person nutzen kann

Scann und Auswertung kann von techafiner (nicht IT) Person nach einer
halbtägigen Schulung durchgeführt werden.

GUI hat Instruktionen für Benutzer, die hilft sinnvolle Daten zu sammeln. (was
das ist müsste noch ausgearbeitet werden)

Link zu https://reports.exodus-privacy.eu.org/de/ hinzufügen.

Installation
--

Packages needed:

- `arp-scan` (file /usr/share/arp-scan/ieee-oui.txt)
- `python3-openssl` (ssl api)
- `mitmproxy` (module CertStore)

Lizenz
--

Meine Teile stehen unter der [GPLv2](LICENSE). Teile die ich übernommen habe,
stehen unter der jeweiligen Lizenz, die Module und die jeweilige Herkunft sind
dokumentiert.

