#!/usr/bin/env -S python3 -u
from logging import getLogger, INFO as LEVEL, basicConfig
from time import sleep
from pathlib import Path
from socket import gethostbyaddr
# see https://rawgit.com/CoreSecurity/pcapy/master/pcapy.html#idp1073147122592 and ../../sniff/sniff.c
from pcapy import open_live, DLT_LINUX_SLL, DLT_EN10MB, DLT_RAW
from ipaddress import IPv4Address
from impacket.ImpactDecoder import EthDecoder, LinuxSLLDecoder, IPDecoder
from impacket.ImpactPacket import ARP, IP
from impacket.IP6 import IP6
from impacket.dns import DNS, DNSFlags
from impacket.dhcp import DhcpPacket
from honeypot.dhcp_l2 import LevelTwo, Opcode # TODO fix impacket and remove this dependency
# see https://dafarry.github.io/tkinterbook/index.htm
# https://en.wikibooks.org/wiki/Python_Programming%2FTkinter
from tkinter import *
from tkinter.ttk import *


log = getLogger(__name__)

class DictObject:
	"""
	class to have a dict which behaves like an object or an object that
	behaves like a dict
	"""
	def __init__(self, **args):
		for key, value in args.items():
			if isinstance(value, dict):
				args[key] = self.__class__(**value)
		self.__dict__.update(**args)

	def __iter__(self):
		yield from self.__dict__.items()

	def items(self):
		return self.__dict__.items()

	def keys(self):
		return self.__dict__.keys()

	def __getitem__(self, key):
		return getattr(self, key)

	def __str__(self):
		return str(dict(self))

	def __repr__(self):
		return repr(dict(self))

class Item():
	"""
	An item to be displayed on the screen.
	"""
	@staticmethod
	def b(f=1):
		return f * 8 # this value scales itemsize and font

	def display(self, canvas, text, sub, bg, px=0, py=0):
		self.canvas = canvas
		self.left_arc = self.canvas.create_arc((0, 0, 1, 1), start=90, extent=180, outline=bg, fill=bg)
		self.rectangle = self.canvas.create_rectangle((0, 0, 1, 1), outline=bg, fill=bg)
		self.righht_arc = self.canvas.create_arc((0, 0, 1, 1), start=-90, extent=180, outline=bg, fill=bg)
		self.main_text = self.canvas.create_text((0, 0), text=text, font=('Sans', self.b(), 'bold', ), anchor=NW, fill='black', )
		self.sub_text = self.canvas.create_text((0, 0), text=sub, font=('Sans', self.b(), ), anchor=SW, fill='#222', )
		self.traffic_text = self.canvas.create_text((0, 0), text="0 bytes", font=('Sans', self.b(), ), anchor=SE, fill='#222', )

	def move(self, px, py):
		self.canvas.coords(self.left_arc, (px+0, py+0, px+self.b(5), py+self.b(5)))
		self.canvas.coords(self.rectangle, (px+self.b(5)//2, py+0, px+self.b(40)+self.b(5)//2, py+self.b(5)))
		self.canvas.coords(self.righht_arc, (px+self.b(40), py+0, px+self.b(40)+self.b(5), py+self.b(5)))
		self.canvas.coords(self.main_text, (px+self.b(5)//2, py+0))
		self.canvas.coords(self.sub_text, (px+self.b(5)//2, py+self.b(5)-2))
		self.canvas.coords(self.traffic_text, (px+self.b(40)-2, py+self.b(5)-2))

class Client(Item):
	"""
	This is a client seen by its DHCP request, ready to be displayed on screen.
	"""
	def __init__(self, canvas, ip, name=""):
		self.ip = ip
		self.byte_count = 0
		self.name = name
		self.connections = dict()
		self.display(canvas, name, ip, "#e90")

	def new_connection(self, canvas, domain, ip):
		""" return true if this is a new connection """
		assert ip != self.ip
		if not (connection := self.connections.get(domain, None)):
			connection = Connection(canvas, domain, ip)
			self.connections[ip] = connection
			log.debug("new_connection %s", self.connections)
			return True

	def new_traffic(self, canvas, ip, byte_count):
		assert ip != self.ip
		self.byte_count += byte_count
		self.canvas.itemconfig(self.traffic_text, text=f"{self.byte_count} bytes")
		if connection := self.connections.get(ip, None):
			connection.new_traffic(canvas, self.ip, byte_count)
		else:
			self.new_connection(canvas, None, ip)
			self.connections[ip].new_traffic(canvas, self.ip, byte_count)
			log.debug("unknown traffic %s %s", ip, byte_count)
			return True

	def get_connections(self):
		yield from self.connections.values()

class Connection(Item):
	"""
	this is one connection (to one IP) from a client seen through its DNS
	request ready to be displayed.
	"""
	BAD_NAME = set()

	def __init__(self, canvas, name, ip):
		self.ip = ip
		self.name = name if name else Connection.reverse(ip)
		self.byte_count = 0
		self.display(canvas, self.name, ip, self.color(name, ip, ))

	@staticmethod
	def reverse(ip):
		try:
			name, alias, addresslist = gethostbyaddr(ip)
			return name
		except:
			return "<unbekannt>"

	@staticmethod
	def is_bad(name, ip):
		return name in Connection.BAD_NAME

	@staticmethod
	def color(name, ip, ):
		log.debug("color %s %s", name, ip)
		if Connection.is_bad(name, ip):
			return "#b11"
		if not name:
			return "#dd0"
		return "#1b1"

	def new_traffic(self, canvas, ip, byte_count):
		self.byte_count += byte_count
		log.debug("new_traffic %s %s %s", ip, self.ip, self.byte_count)
		self.canvas.itemconfig(self.traffic_text, text=f"{self.byte_count} bytes")

class State():
	"""
	This is a singleton which takes all clients seen.
	"""
	CLIENTS = dict()

	@classmethod
	def new_client(cls, canvas, ip, name):
		client = cls.CLIENTS.get(ip, None)
		if not client:
			client = Client(canvas, ip, name)
			cls.CLIENTS[ip] = client
			cls.render(canvas)
		return client

	@classmethod
	def new_connection(cls, canvas, ip, name, answer):
		if client := cls.CLIENTS.get(str(ip), None):
			log.debug("client %s connects %s %s", ip, name, ip)
			if client.new_connection(canvas, name, answer):
				cls.render(canvas)
		else:
			log.warning("no client %s %s %s %s", ip, name, answer, cls.CLIENTS)

	@classmethod
	def new_traffic(cls, canvas, ip1, ip2, count):
		if client := cls.CLIENTS.get(ip1, None):
			if client.new_traffic(canvas, ip2, count):
				cls.render(canvas)
		elif client := cls.CLIENTS.get(ip2, None):
			if client.new_traffic(canvas, ip1, count):
				cls.render(canvas)
		else:
			log.warning("traffic from unkown client %s -> %s, %s bytes", ip1, ip2, count)

	@classmethod
	def trigger(cls, your_ip_address, domain=None, answer=None, hostname=None, **argv):
		client = State.get_client(your_ip_address, True)
		if domain:
			client.connect(domain, answer) # answer: Name, IPAddress

	@classmethod
	def render(cls, canvas):
		height = canvas.winfo_height()
		x, y = 0, 0
		for client in cls.CLIENTS.values():
			log.debug("move client")
			client.move(x, y)
			for connection in client.get_connections():
				log.debug("move connection")
				y += Item.b(5)
				if y + Item.b(5) > height:
					y = Item.b(5)
					x += Item.b(40+5+1)
				connection.move(x+Item.b(3), y)
			#y += Item.b(6) # one column
			x, y = x+Item.b(40+5+1), 0 # column per client


class HandlerBase:
	"""
	An abstract handler for traffic.
	"""
	def work(self, hdr, ip):
		if ip.get_ip_p() == 6: # TCP
			tcp = ip.child()
			# tcp.get_ACK() tcp.get_CWR() tcp.get_ECE() tcp.get_FIN() tcp.get_PSH() tcp.get_RST() tcp.get_SYN() tcp.get_URG()
			log.debug("TCP %s:%s -> %s:%s",
					ip.get_ip_src(), tcp.get_th_sport(),
					ip.get_ip_dst(), tcp.get_th_dport(),
					)
			# DNS
			if tcp.get_th_dport() == 53:
				log.debug("tcp port 53")
				self.learn_dns(IPv4Address(ip.get_ip_src()), tcp.child().get_packet()[2:])
			elif tcp.get_th_sport() == 53:
				log.debug("tcp port 53")
				self.learn_dns(IPv4Address(ip.get_ip_dst()), tcp.child().get_packet()[2:])
		elif ip.get_ip_p() == 1: # ICMP
			log.debug("ICMP",
					ip.get_ip_src(),
					ip.get_ip_dst(),
					)
		elif ip.get_ip_p() == 17: # UDP
			udp = ip.child()
			log.debug("UDP %s:%s -> %s:%s",
					ip.get_ip_src(), udp.get_uh_sport(),
					ip.get_ip_dst(), udp.get_uh_dport(),
					)
			# DHCP
			if udp.get_uh_dport() == 67 or udp.get_uh_sport() == 67 or udp.get_uh_dport() == 68 or udp.get_uh_sport() == 68:
				self.learn_dhcp(udp.child().get_packet())
			# DNS
			if udp.get_uh_dport() == 53:
				log.debug("udp port 53")
				self.learn_dns(IPv4Address(ip.get_ip_src()), udp.child().get_packet())
			elif udp.get_uh_sport() == 53:
				log.debug("udp port 53")
				self.learn_dns(IPv4Address(ip.get_ip_dst()), udp.child().get_packet())
		else:
			log.debug("unknown ip proto %s", ip.get_ip_p())
		State.new_traffic(self.canvas, ip.get_ip_src(), ip.get_ip_dst(), len(ip.get_packet()), )

	def learn_dhcp(self, buffer):
		dhcp = DictObject(**LevelTwo.decode(buffer))
		log.debug("dhcp=%s", dhcp)
		if dhcp.opcode == Opcode.REPLY:
			State.new_client(self.canvas, str(dhcp.your_ip_address), "") # TODO retrieve name from dhcp buffer
		return
		# code with impacket doesnt work:
		try:
			dhcp = DhcpPacket(buffer)
			log.debug("dhcp=%s", dhcp)
		except Exception as e:
			log.exception("buffer=%s", buffer)


	def learn_dns(self, requestor, buffer):
		try:
			log.debug("dns")
			dns = DNS(buffer)
			if dns.get_flags() & DNSFlags.QR_RESPONSE:
				log.debug("dns is RESPONSE %s: %s", dns, dns.get_answers())
				for domain, _, _, _, answer in dns.get_answers():
					log.debug("dns %s %s", domain, answer)
					if "IPAddress" in answer:
						State.new_connection(self.canvas, requestor, domain.decode(), answer["IPAddress"])
					elif "Name" in answer:
						log.info("alias %s %s", domain.decode(), answer["Name"])
					else:
						log.warning("learn_dns no IPAddress or Name %s %s", domain, answer)
#			else: # show name already before IP is known:
#				for domain, _, _ in dns.get_questions():
#					State.new_connection(self.canvas, requestor, domain.decode())
		except Exception as e:
			log.exception("buffer=%s", buffer)


class RawHandler(HandlerBase):
	"""
	A concrete handler of traffic as raw snoop
	"""
	def __init__(self, canvas):
		self.decoder = IPDecoder()
		self.canvas = canvas

	def __call__(self, hdr, data):
		ip = self.decoder.decode(data)
		self.work(hdr, ip)

class LinuxSLLHandler(HandlerBase):
	"""
	A concrete handler of traffic as LinuxSLL snoop
	"""
	def __init__(self, canvas):
		self.decoder = LinuxSLLDecoder()
		self.canvas = canvas

	def __call__(self, hdr, data):
		# TODO implement & test LinuxSLL
		raise

class EN10MBHandler(HandlerBase):
	"""
	A concrete handler of traffic as EN10MB snoop
	"""
	def __init__(self, canvas):
		self.decoder = EthDecoder()
		self.canvas = canvas

	def __call__(self, hdr, data):
		p = self.decoder.decode(data)
		if p.get_ether_type() == IP.ethertype:
			ip = p.child()
			self.work(hdr, ip)
		elif p.get_ether_type() == ARP.ethertype:
			arp = p.child()
			log.debug("ARP %s %s %s %s %s",
					arp.get_op_name(arp.get_ar_op()),
					arp.as_hrd(arp.get_ar_sha()),
					arp.as_hrd(arp.get_ar_tha()),
					arp.as_pro(arp.get_ar_spa()),
					arp.as_pro(arp.get_ar_tpa()),
					)
		elif p.get_ether_type() == IP6.ethertype:
			log.debug("IPv6 %s", p)
		else:
			log.debug("unknown ether type %s", hex(p.get_ether_type()))
			# see https://en.wikipedia.org/wiki/EtherType

class Visualizer(Frame):
	def __init__(self, interface, *ips):
		self.root = Tk()
		#self.root.attributes('-fullscreen', True)
		height = self.root.winfo_vrootheight()
		width = self.root.winfo_vrootwidth()
		for n in ("<Control-q>", "<Control-w>", "<Control-x>", "<Alt-F4>", "q"):
			self.root.bind_all(n, self.do_quit)
		Frame.__init__(self, self.root)
		self.canvas = Canvas(self.root, width=width, height=height, cursor="none", borderwidth=0, background="#333", highlightthickness=0, )
		self.canvas.pack()
		self.canvas.delete(ALL)
		self.prepare(interface)
		self.after(100, self.process)
		# TODO remove hardcoded test clients:
		for ip in ips:
			State.new_client(self.canvas, ip, "Gerät")

	def do_quit(self, *args):
		' exit program '
		self.root.quit()

	def prepare(self, interface, filter_=None):
		log.debug("prepare")
		self.pcap = open_live(interface, 8000, True, 100)
		if filter_:
			self.pcap.setfilter(filter_)
		self.pcap.set_snaplen(1500)
		#self.pcap.set_promisc(1)
		self.pcap.setnonblock(1)
		datalink = self.pcap.datalink()
		if DLT_EN10MB == datalink:
			self.decoder = EN10MBHandler(self.canvas)
		elif DLT_LINUX_SLL == datalink:
			self.decoder = LinuxSLLHandler(self.canvas)
		elif DLT_RAW == datalink:
			self.decoder = RawHandler(self.canvas)
		else:
			raise Exception("Datalink type not supported: {}".format(datalink))
		log.info("prepare done")

	def process(self):
		log.debug("process")
		self.pcap.dispatch(-1, self.decoder)
		self.after(20, self.process)

def main(interface='wlan0', *ips):
	' main start point of the program '
	badlist = Path("hosts_full")
	if badlist.exists():
		Connection.BAD_NAME = set((name.strip() for name in badlist.read_text().split("\n")))
	else:
		log.warning("badlist %s doesnt exist", badlist)
	app = Visualizer(interface, *ips)
	app.mainloop()
	app.root.destroy()

if __name__ == '__main__':
	from sys import stderr
	basicConfig(stream=stderr, level=LEVEL, format='%(asctime)s %(levelname)s: %(message)s')
	from sys import argv
	#cap(*argv[1:])
	main(*argv[1:])
# vim:tw=0:nowrap
