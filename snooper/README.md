Snooper
==

Da ein Honeypod nach der ersten Idee aus [honeypot](../honeypot) wegen der
Verschlüsselung nicht zielführend ist, habe ich eine andere Idee. Wir "snoopen"
einfach den Verkehr. Das heisst, wir lauschen einfach mit, lassen den
vorhandenen Router alles tun und ziehen unsere Schlüsse. Wir können zwar nicht
in die Pakete schauen, die Meta-Daten verraten uns aber genug für eine nette
Visualisierung.

Abhängigkeiten
==

Folgende Pakete sind nötig:

- **tkinter**: Für die grafische Oberfläche
- **pcapy**: Um den Netzwerkverkehr zu schnüffeln
- **ipaddress**: Um mit IP-Adressen zu arbeiten
- **impacket**: Um Pakete zu lesen. Die aktuelle ("HEAD") Version von
  [github](https://github.com/fortra/impacket) hat Bugs, besonders einen im
  DHCP Parser, der uns trifft, darum brauchen wir auch:
- **honeypot**: Teil dieses Projektes, es enthält Code zum parsen von DHCP.

Ein Symlink erlaubt das Laden der nötigen Module vom honeypot Projekt (die
leider nötig sind, da impacket den einen oder andren Bug hat):

```
ln -s ../honeypot .
```

Schlechte Namen
==

Verschiedene Projekte kümmern sich darum, Namen von Stalkerware Rechnern im Internet zu sammeln, zum Beispiel diese:

[hosts full](https://raw.githubusercontent.com/AssoEchap/stalkerware-indicators/master/generated/hosts_full)

Diese Datei wird genutzt, um Zugriffe als "schlecht" darzustellen (rot). Ich lade sie per

```
curl -O https://raw.githubusercontent.com/AssoEchap/stalkerware-indicators/master/generated/hosts_full
```

Start
==

Leider muss das Program als Benutzer root laufen, da es sonst nicht snoopen darf.
Das führt zu dem Problem, dass root unter Umständen keine Grafik-Programme
starten darf. Die nächsten Schritte werden als Benutzer root ausgeführt, um
dies Möglich zu machen.

Die Einschätzung, ob die folgenden Befehle zu unsicher sind, eine Gefahr
darstellen oder mein System kompromitieren obligt dir. Wenn dir irgendetwas zu
gefährlich vorkommt, lass es sein.

Zunächst fehlt die Autorisierung. Ich stelle sie mit einem Symlink her
(das "#"-Zeichen ist der Prompt der Shell und muss nicht mit eingegeben werden,
`<user>` ersetzen durch meinen normalen Benutzer):

```
# ln -s /home/<user>/.Xauthority .
```

Dann muss die DISPLAY Variable gesetzt werden, damit die Verbindung hergestellt
werden kann:

```
# export DISPLAY=:0
```

Schliesslich den PYTHONPATH setzen und das Program starten (dabei den Namen des
Gerätes und die IP anpassen):

```
# export PYTHONPATH=.
# python3 ./main.py eth0 192.168.0.1
```

Zu tun
==

- SNI
- namen die zu gleichenn IPs auflösen
- name der zu mehreren IPs auflöst

