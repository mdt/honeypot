#!/bin/sh -e
DEVICENAME=accesspoint
apt install hostapd iw
iw dev ${DEVICENAME} del || true
ifdown wlan0 || true
/etc/init.d/wpa_supplicant stop || true
rfkill unblock wlan && sleep 1
iw phy $(iw list|awk '/^Wiphy/{print $2}') interface add ${DEVICENAME} type __ap
ip link set dev ${DEVICENAME} up
# ip address add dev $(DEVICENAME) 10.0.0.1/24
# or:
# ip link add name honey type bridge
# ip link set dev honey up
# ip link set eth0 master honey
# ip link set wlan0 master honey
# # ip link delete honey type bridge
# iptables -P FORWARD DROP
# and:
# iptables -A FORWARD -s $IP -j ACCEPT
# iptables -A FORWARD -d $IP -j ACCEPT
# https://wiki.archlinux.org/title/Iptables
cat > hostapd.conf <<EOF
interface=${DEVICENAME}
driver=nl80211
ssid=open-net
channel=7
hw_mode=g
wme_enabled=1
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=3
wpa_passphrase=supergeheim
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
logger_stdout=-1
logger_stdout_level=1
EOF
hostapd hostapd.conf -P hostapd.pid -dd
# hostapd_cli
iw dev ${DEVICENAME} del
rfkill block wlan
