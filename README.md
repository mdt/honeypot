Projekt Honigtopf
=================

Was macht mein Gerät da im Dunkeln?
-----------------------------------

Heute weisst du nicht, was dein Computer tut, sei es Notebook, Handy oder Pad.
Wohin verbindet es, welche Daten verrät es? Programmen auf dem selben Gerät
kannst du nicht trauen, wenn das Gerät kompromittiert ist. Der einzige Weg ist,
es "von aussen" zu bepbachten. Dieses Projekt ist dafür da.

Es enthält zwei Ansätze: Eins ist ein typischer honeypot, bei dem sämtlicher
Datenverkehr mit dem honeypot stattfindet und so analysiert werden kann. Ein
zweiter ist, den Verkehr lediglich mitzuschnüffeln und zu analysieren. Der
zweite Ansatz erlaubt nicht, den Inhalt der Kommunikation zu analysieren, wenn
er verschlüsselt ist.

Honig
-----

Diesen Ansatz findest du in [honeypot](honeypot/README.md).

Schnüffel
---------

Diesen Ansatz findest du in [snooper](snooper/README.md).

Mehr Quellen
------------

- [androguard](https://androguard.readthedocs.io/en/latest/)
- [antistalking](https://antistalking.haecksen.org/)
- [mitmproxy](https://mitmproxy.org)
- [mvt](https://docs.mvt.re/en/latest/)
- [spytrap adb](https://github.com/spytrap-org/spytrap-adb)
- [spytrap wifi](https://github.com/spytrap-org/spytrap-wifi)
- [stalkerware indicators](https://github.com/AssoEchap/stalkerware-indicators/)
- [virustotal](https://www.virustotal.com/gui/home/upload)

