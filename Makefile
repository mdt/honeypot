# see https://mitmproxy.org/
# wireshark filter: !udp.port == 53 && !(ip.addr == 13.248.212.111) && !(ip.addr == 76.223.92.165) && !arp && !dhcp
# curl -v --cacert pemdb/open.net-ca-cert.cer --connect-to emdete.de:443:172.16.66.1:443 https://emdete.de

all: build

run: honey

dbg:
	# push to all known remotes
	$(foreach remote,$(shell git remote),$(shell git push $(remote)))

honey:
	# run the honey pot
	PYTHONPATH=lib \
	python3 -u -m honeypot

clean:
	rm -rf pemdb/ dist/ honeypot.egg-info/ snoops/

build:
	python3 -m build

package_check:
	# run full test for stalkerware
	./packages.py

no_captive_portal:
	# switch off the captive portal test
	# see https://www.kuketz-blog.de/android-captive-portal-check-204-http-antwort-von-captiveportal-kuketz-de/
	adb shell settings put global captive_portal_mode 0

ioc.yaml:
	# retrieve ioc into a local file
	rm -f ioc.yaml && curl -O https://raw.githubusercontent.com/AssoEchap/stalkerware-indicators/master/ioc.yaml

packages.txt:
	# retrieve packages into a local file
	adb shell pm list packages -f | sort | tee $@

pemdb/open.net-ca-cert.key: Makefile
	mkdir pemdb
	openssl genrsa -out $@ 4096

/tmp/open.net.csr: pemdb/open.net-ca-cert.key
	openssl req \
		-new \
		-nodes \
		-key $< \
		-subj "/C=DE/ST=Berlin/L=Berlin/O=Open Net CA/OU=Certification/CN=open.net"\
		-out $@

pemdb/open.net-ca-cert.cer: pemdb/open.net-ca-cert.key /tmp/open.net.csr
	openssl x509 \
		-req \
		-days 365 \
		-in /tmp/open.net.csr \
		-signkey pemdb/open.net-ca-cert.key \
		-out $@

pemdb/open.net.dhparam:
	openssl dhparam -out $@ 2048

show: pemdb/open.net-ca-cert.cer
	openssl x509 -in pemdb/open.net-ca-cert.cer -noout -text
	#curl -v --cacert pemdb/open.net-ca-cert.cer --connect-to localhost:443:172.16.66.1:443 https://localhost

push: pemdb/open.net-ca-cert.cer
	# push the honey pots certificate to the phone to be able to add it
	adb push $@ /sdcard/Downloads/.

